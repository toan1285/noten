/**
 * Created by toan on 07.11.2016.
 */

var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    plumber = require('gulp-plumber'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

var pubFolder = 'public',
    appFolder = 'app',
    autoprefixerOptions = {
        browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
    },
    sassOptions = {
        errLogToConsole: true,
        outputStyle: 'expanded'
    };

/**
 * scripts tasks
 */
gulp.task('bowerComponents', function() {
    gulp.src([ appFolder+'/bower_components/**/*.min.js', appFolder+'/bower_components/**/*-min.js'] )
        .pipe(gulp.dest(pubFolder+'/js/bower_components'));
});

gulp.task('scripts', function() {
    gulp.src(appFolder+'/js/**/*.js')
        .pipe(plumber())
        .pipe(gulp.dest(pubFolder+'/js'))
        .pipe(reload({stream: true}));

});
gulp.task('appJS', function() {
    gulp.src(appFolder + '/app.js')
        .pipe(plumber())
        .pipe(gulp.dest(pubFolder))
        .pipe(reload({stream: true}));
});
/**
 * sass tasks
 */
gulp.task('sass', function () {
    return gulp.src(appFolder+'/scss/**/*.scss')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass(sassOptions).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(gulp.dest(pubFolder+'/css'))
        .pipe(reload({stream:true}))
        .resume();
});

/**
 * html
 */
gulp.task('html', function() {
    gulp.src(appFolder+'/**/*.html')
        .pipe(gulp.dest(pubFolder))
        .pipe(reload({stream:true}));
});


/**
 * browser-sync
 */
gulp.task('browser-sync', function () {
   browserSync({
       server: {
           baseDir: './'+pubFolder
       }
   })
});

/**
 * watch
 */
gulp.task('watch', function() {
    gulp.watch(appFolder+'/**/*.html', ['html']);
    gulp.watch(appFolder+'/app.js', ['appJS']);
    gulp.watch(appFolder+'/js/**/*.js', ['scripts']);
    gulp.watch(appFolder+'/scss/**/*.scss', ['sass'])
    .on('change', function(event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});

gulp.task('default', ['appJS', 'scripts', 'bowerComponents' , 'sass', 'html', 'browser-sync' , 'watch' ]);
