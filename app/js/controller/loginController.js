/**
 * Created by toan on 08.11.2016.
 */

app.controller('loginCtrl', function($rootScope, $scope, $location, $http) {
    $scope.submit = function (isValid) {

        //todo: http-request auf api

        if(isValid) {
            var req = {
                method: 'POST',
                url: 'http://localhost/noten/api/login',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    user: $scope.username,
                    pass: $scope.password

                }
            }

            $http(req)
                .then(function (response) {
                    if (response.data.login) {
                        $rootScope.loginState = true;
                        if(response.data.firstname !== null && response.data.firstname !== '' && response.data.firstname !== undefined) {
                            $rootScope.userName = response.data.firstname;
                        } else {
                            $rootScope.userName = response.data.username;
                        }
                        $location.path('/dashboard');
                    }
                });
        }
    }

});