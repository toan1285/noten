'use strict';

var app = angular.module('app', [
    'ngRoute',
    'ngAnimate'
]);


app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/login'
        })
        .when('/dashboard', {
            resolve: {
                  "check" : function($rootScope, $location) {
                        if(!$rootScope.loginState) {
                            $location.path('/');
                        }
                  }
            },
            templateUrl: 'views/dashboard'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);



