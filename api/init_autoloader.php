<?php
/**
 * Created by PhpStorm.
 * User: toan
 */


function loadClass($classname) {
    $f = implode(DIRECTORY_SEPARATOR, explode('\'', $classname)) . '.php';
    if(file_exists($f)) {
        include_once ($f);
        return true;
    }
}

spl_autoload_register('loadClass');