-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 16. Nov 2016 um 14:00
-- Server-Version: 5.6.24
-- PHP-Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `noten`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- TRUNCATE Tabelle vor dem Einfügen `companies`
--

TRUNCATE TABLE `companies`;
--
-- Daten für Tabelle `companies`
--

INSERT INTO `companies` (`id`, `name`, `user_id`) VALUES
(1, 'plazzAG', 6),
(2, 'Ruhlamat', 7),
(3, 'Takwa', 10),
(4, 'PSL', 11);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `grades`
--

CREATE TABLE IF NOT EXISTS `grades` (
  `id` int(10) unsigned NOT NULL,
  `shortname` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `teacher` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- TRUNCATE Tabelle vor dem Einfügen `grades`
--

TRUNCATE TABLE `grades`;
--
-- Daten für Tabelle `grades`
--

INSERT INTO `grades` (`id`, `shortname`, `name`, `teacher`) VALUES
(1, 'FIA14 - Eng', 'FIA14 - English', 3),
(2, 'FIA14 - DK', 'FIA14 - Deutsch Kommunikation', 5),
(3, 'FIS14 -Eng', 'FIS14 - Englisch', 3),
(4, 'FIS14- DK', 'FIS14 - Deutsch Kommunikation', 5);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `grade_has_pupil`
--

CREATE TABLE IF NOT EXISTS `grade_has_pupil` (
  `grade_id` int(11) NOT NULL,
  `pupil_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- TRUNCATE Tabelle vor dem Einfügen `grade_has_pupil`
--

TRUNCATE TABLE `grade_has_pupil`;
--
-- Daten für Tabelle `grade_has_pupil`
--

INSERT INTO `grade_has_pupil` (`grade_id`, `pupil_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(1, 8),
(1, 9),
(3, 8),
(3, 9);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `pupil`
--

CREATE TABLE IF NOT EXISTS `pupil` (
  `id` int(10) unsigned NOT NULL,
  `company` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- TRUNCATE Tabelle vor dem Einfügen `pupil`
--

TRUNCATE TABLE `pupil`;
--
-- Daten für Tabelle `pupil`
--

INSERT INTO `pupil` (`id`, `company`, `user_id`) VALUES
(1, 6, 1),
(2, 6, 2),
(3, 10, 8),
(4, 11, 9);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `score`
--

CREATE TABLE IF NOT EXISTS `score` (
  `score_id` int(10) unsigned NOT NULL,
  `pupil_id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `desc_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- TRUNCATE Tabelle vor dem Einfügen `score`
--

TRUNCATE TABLE `score`;
--
-- Daten für Tabelle `score`
--

INSERT INTO `score` (`score_id`, `pupil_id`, `grade_id`, `score`, `desc_id`) VALUES
(1, 1, 2, 1, 1),
(2, 1, 2, 1, 2),
(3, 2, 2, 2, 1),
(4, 2, 2, 1, 2),
(5, 8, 3, 2, 3),
(6, 9, 3, 2, 1),
(7, 8, 3, 1, 4),
(8, 9, 3, 1, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `score_desc`
--

CREATE TABLE IF NOT EXISTS `score_desc` (
  `id` int(10) unsigned NOT NULL,
  `desc` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- TRUNCATE Tabelle vor dem Einfügen `score_desc`
--

TRUNCATE TABLE `score_desc`;
--
-- Daten für Tabelle `score_desc`
--

INSERT INTO `score_desc` (`id`, `desc`) VALUES
(1, 'Arbeit 1'),
(2, 'Arbeit 2'),
(3, 'Irgend ein Projekt'),
(4, 'Komische Arbeit über sinnlosen Scheiß');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `token`
--

CREATE TABLE IF NOT EXISTS `token` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

--
-- TRUNCATE Tabelle vor dem Einfügen `token`
--

TRUNCATE TABLE `token`;
--
-- Daten für Tabelle `token`
--

INSERT INTO `token` (`id`, `user_id`, `time`, `token`) VALUES
(38, 1, 1479301906, '582c578eee8f5'),
(39, 2, 1479301222, '582c54e28e9cd');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `user` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `role` int(11) DEFAULT NULL,
  `lastlogin` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- TRUNCATE Tabelle vor dem Einfügen `user`
--

TRUNCATE TABLE `user`;
--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `user`, `pass`, `role`, `lastlogin`) VALUES
(1, 'Torsten', 'Ansorg', 'ta@plazz.ag', '05e037c57cdc9fe20f7a97c5af652a0f84acb8f9', 2, '2016-11-16 12:28:40'),
(2, 'Maximilian', 'Raupach', 'schueler1', '332ad086941c4c3d7a125c295abe801f83e59370', 2, '2016-11-14 08:05:47'),
(3, 'Der', 'Lehrer', 'lehrer1', '332ad086941c4c3d7a125c295abe801f83e59370', 3, '2016-11-14 08:05:42'),
(5, 'Der 2. ', 'Lehrer', 'lehrer2', '245dc148cfe2f0a29dd200c96765399bfdfb37ae', 3, '2016-11-14 09:29:11'),
(6, 'plazz', 'AG', 'info@plazz.ag', 'c0223c5113bf5c36fc4a6ec6b8ae64fcc902c76d', 1, '2016-11-15 12:54:45'),
(7, 'Ruhla', 'mat', 'info@ruhlamat.de', '5d6316fc48bee9e0e0d6e54849dbbf626513fbd7', 1, '2016-11-15 12:55:09'),
(8, 'Danielle', 'Noeller', 'dnoeller', 'abcdef', 2, NULL),
(9, 'Maximilian', 'Peetz', 'mpeetz', 'rwtqr', 2, NULL),
(10, 'Tak', 'Wa', 'takwa@info.net', 'dsgakjfösdf', 1, NULL),
(11, 'PS', 'L', 'psl@info.net', 'sdrghasdh', 1, NULL);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `pupil`
--
ALTER TABLE `pupil`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `score`
--
ALTER TABLE `score`
  ADD PRIMARY KEY (`score_id`);

--
-- Indizes für die Tabelle `score_desc`
--
ALTER TABLE `score_desc`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`), ADD UNIQUE KEY `token` (`token`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`), ADD UNIQUE KEY `user` (`user`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT für Tabelle `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT für Tabelle `pupil`
--
ALTER TABLE `pupil`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT für Tabelle `score`
--
ALTER TABLE `score`
  MODIFY `score_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT für Tabelle `score_desc`
--
ALTER TABLE `score_desc`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT für Tabelle `token`
--
ALTER TABLE `token`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
