<?php
/**
 * Created by PhpStorm.
 * User: toan
 *
 *  all api requests will be redirected to this file
 *  if client calls login request and login is a valid the LoginController will create a token which is returned to the client
 *  for further requests within the next 15 minutes the client will have to use that token, otherwise permission is denied
 *
 *  the autoloader will dynamically include needed php-classes for processing the request
 *
 *  global request stores all request information
 *  global response stores information for the api response (HeaderInformation, StatusCode, Body ...)
 *  global user object defines the permissions if a valid user token is sent and a valid user is logged in
 *
 *  after processing the requested uri the api will return data as body-text (JSON)
 *  if there's an exception (no permission, no method, ... ) the api will set the Respond Headers and return the Error-Message as body-text
 */

use helper\RequestHelper;
use helper\ResponseHelper;
use helper\UserHelper;
use exceptions\Exception;
use enums\HeaderFields;
use enums\StatusCodes;





include_once 'init_autoloader.php';

$user = new UserHelper();
$request = new RequestHelper();
$response = new ResponseHelper();


try {
    if(strtolower($request->getUrlElements()[2] !== 'login')) {
        if(!$user->hasValidToken()) {
            throw new Exception('User has to be authed', StatusCodes::UNAUTHORIZED);
        }
    }
    $controller = $request->getUrlElements()[1].DIRECTORY_SEPARATOR.ucfirst($request->getUrlElements()[2].'Controller');
    $controller = new $controller($request->getUrlElements(), $request->getParameters());
    $response->registerHeader(HeaderFields::CONTENT_TYPE, $request->getRequestFormat());

}
catch (Exception $ex) {
    $response->setStatusCode($ex->getCode());
    $response->setBody($ex->getMessage());
}

echo $response->returnResponse();

