<?php
/**
 * Created by PhpStorm.
 * User: fia15a-dnoeller
 * Date: 15.11.2016
 * Time: 13:42
 */

namespace controllers;


use enums\Roles;
use enums\StatusCodes;
use exceptions\Exception;
use mapper\GradesTable;
use mapper\ScoreTable;

class ScoreController extends BaseController {

    /**
     * ScoreController constructor.
     * @param $uri
     * @param null $param
     */
    public function __construct($uri, $param = null)
    {
        parent::__construct($uri, $param);
    }

    /**
     * @return array
     */
    private function getGradeIds(){
        $gt = new GradesTable();
        $gra = $gt->getAllGrades($GLOBALS['user']->getUserId());
        $gradeIds = [];
        foreach($gra as $g) {
            array_push($gradeIds, $g['id']);
        }
        return $gradeIds;
    }

    /**
     * @return bool
     * lists all scores for the specific logged in user
     */
    public function indexAction(){
        $st = new ScoreTable();
        $data = $st->getScore($GLOBALS['user']->getUserId());
        return $this->restOutputJSON($data);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function editAction() {
        if($GLOBALS['user']->getRole() === Roles::TEACHER) {
            if(isset($this->param['score_id']) && isset($this->param['score'])) {
                $s = [
                    'score_id' => $this->param['score_id'],
                    'score' => $this->param['score'],
                ];
                $st = new ScoreTable();
                $data = $st->editScore($s);
                return $this->restOutputJSON($data);
            }
            throw new Exception('Missing Arguments', StatusCodes::BAD_REQUEST);
        }
        throw new Exception('Unauthorized', StatusCodes::UNAUTHORIZED);
    }

    /**
     * @return bool|void
     * @throws Exception
     */
    public function createAction() {
        if($GLOBALS['user']->getRole() === Roles::TEACHER) {
            if(isset($this->param['grade_id']) && isset($this->param['pupil_id']) && isset($this->param['score_desc']) && isset($this->param['score'])) {
                if(!in_array($this->param['grade_id'], self::getGradeIds())) {
                    throw new Exception('Not allowed, not your grade', StatusCodes::FORBIDDEN);
                    return;
                }
                $st = new ScoreTable();
                $s = [
                    'pupil_id' => $this->param['pupil_id'],
                    'grade_id' => $this->param['grade_id'],
                    'score' => $this->param['score'],
                    'score_desc' => $this->param['score_desc']

                ];
                $data = $st->createScore($s);
                return $this->restOutputJSON($data);
            }
            throw new Exception('Missing Arguments', StatusCodes::BAD_REQUEST);
        }
        throw new Exception('Unauthorized', StatusCodes::UNAUTHORIZED);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function deleteAction() {
        if($GLOBALS['user']->getRole() === Roles::TEACHER) {
            if(isset($this->param['score_id'])) {
                $st = new ScoreTable();
                $data = $st->deleteScore($this->param['score_id']);
                return $this->restOutputJSON($data);
            }
            throw new Exception('Missing Arguments', StatusCodes::BAD_REQUEST);
        }
        throw new Exception('Unauthorized', StatusCodes::UNAUTHORIZED);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function testsAction() {
        if($GLOBALS['user']->getRole() === Roles::TEACHER) {
            $st = new ScoreTable();
            $data = $st->getTestsFromGrades(self::getGradeIds());
            return $this->restOutputJSON($data);
        }
        throw new Exception('Unauthorized', StatusCodes::UNAUTHORIZED);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function createtestAction () {
        if($GLOBALS['user']->getRole() === Roles::TEACHER) {
            if(isset($this->param['grade_id']) && isset($this->param['grade_desc'])) {
                $st = new ScoreTable();
                $data = $st->createTest($this->param['grade_id'], $this->param['grade_desc']);
                return $this->restOutputJSON($data);
            }
            throw new Exception('Missing Arguments', StatusCodes::BAD_REQUEST);
        }
        throw new Exception('Unauthorized', StatusCodes::UNAUTHORIZED);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function deletetestAction() {
        if($GLOBALS['user']->getRole() === Roles::TEACHER) {
            if(isset($this->param['test_id'])) {
                $sc = new ScoreTable();
                return $this->restOutputJSON($sc->deleteTest($this->param['test_id']));
            }
            throw new Exception('Missing Arguments', StatusCodes::BAD_REQUEST);
        }
        throw new Exception('Unauthorized', StatusCodes::UNAUTHORIZED);
    }

}