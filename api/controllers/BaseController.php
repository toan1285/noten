<?php
/**
 * Created by PhpStorm.
 * User: toan
 */

namespace controllers;
use \exceptions\Exception;
use \enums\StatusCodes;
use enums\HeaderFields;


class BaseController
{
    protected $action = null;
    protected $param;


    /**
     * @return mixed
     */
    protected function getParam() {
        return $this->param;
    }

    /**
     * BaseController constructor.
     * @param $uri
     * @param $parameters
     * @throws Exception
     */
    public function __construct($uri, $parameters)
    {

        if(isset($uri[3])) {
            $this->action = strtolower($uri[3]) .'Action';
        } else {
            $this->action = 'indexAction';
        }

        if(isset($parameters)) {
            $this->param = $parameters;
        }

        if(method_exists($this, $this->action)) {
            call_user_func([$this, $this->action]);
        } else {
            throw new Exception('Method couldn\'t be found.', StatusCodes::NOT_FOUND);
        }

    }

    /**
     * @param $data
     * @return bool
     */
    public function restOutputJSON($data) {
        $GLOBALS['response']->registerHeader(HeaderFields::CONTENT_TYPE, $GLOBALS['request']->getRequestFormat());
        $GLOBALS['response']->setStatusCode(StatusCodes::OK);
        $GLOBALS['response']->setBody(json_encode($data));
        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    public function restOutputXML($data) {
        // just for ur info .. it would be possible to return data in a different format then json
        return false;
    }
}