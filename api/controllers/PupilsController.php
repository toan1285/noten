<?php
/**
 * Created by PhpStorm.
 * User: fia15a-tansorg
 * Date: 15.11.2016
 * Time: 08:50
 */

namespace controllers;


use enums\Roles;
use enums\StatusCodes;
use exceptions\Exception;
use mapper\PupilsTable;

class PupilsController extends BaseController
{
    /**
     * PupilsController constructor.
     * @param $uri
     * @param null $param
     */
    public function __construct($uri, $param = null)
    {
        parent::__construct($uri, $param);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function indexAction()
    {
        if ($GLOBALS['user']->getRole() !== null && $GLOBALS['user']->getRole() !== Roles::PUPIL) {
            $pt = new PupilsTable();
            $gradeId = !is_null($this->param['grade_id']) ? $this->param['grade_id'] : null;
            if ($GLOBALS['user']->getUserId()) {
                return $this->restOutputJSON($pt->getPupils($gradeId));
            }
        }
        throw new Exception('Unauthorized', StatusCodes::UNAUTHORIZED );
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function editAction() {
        $pt = new PupilsTable();
        if($GLOBALS['user']->getRole() === Roles::ADMIN) {
            if (isset($this->param['pupil_id'])) {
                return $this->restOutputJSON($pt->editPupil($this->param['pupil_id'], $this->param));
            }
            throw new Exception('No Pupil-ID was given', StatusCodes::BAD_REQUEST);
        } else if($GLOBALS['user']->getRole() === Roles::PUPIL) {
            return $this->restOutputJSON($pt->editPupil($GLOBALS['user']->getUserId(), $this->param));
        }
        throw new Exception('Unauthorized', StatusCodes::UNAUTHORIZED);
    }

}