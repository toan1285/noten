<?php
/**
 * Created by PhpStorm.
 * User: toan
 */

namespace controllers;
use enums\StatusCodes;
use exceptions\Exception;
use mapper\TokenTable;
use \mapper\UserTable;


use exceptions\Exceptions;

class LoginController extends BaseController
{
    /**
     * LoginController constructor.
     * @param $uri
     * @param null $param
     */
    public function __construct($uri, $param = null)
    {
        parent::__construct($uri, $param);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function indexAction() {
        $data = null;

        if(!is_null($this->getParam()) && isset($this->param['user']) && isset($this->param['pass'])) {
            $ut = new UserTable();
            $data = $ut->login($this->param['user'], $this->param['pass']);
            if(isset($data['user']) && isset($data['id'])) {
                $tt = new TokenTable();
                $tt->createUserToken($data['id']);
                $data['token'] = $tt->getUserToken($data['id']);
                $data['login'] = true;
                return $this->restOutputJSON($data);
            };
            throw new Exception('No valid Login', StatusCodes::NOT_FOUND);
        }
        throw new Exception('Unauthorized: no valid login Data', StatusCodes::UNAUTHORIZED);

    }


    /*
    // just for creating purposes :)

    public function createAction() {
        $ut = new UserTable();
        $ut->createUser($this->param);
    }
    */

}