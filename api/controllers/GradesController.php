<?php
/**
 * Created by PhpStorm.
 * User: FIA15A-DNoeller
 * Date: 14.11.2016
 * Time: 10:10
 */

namespace controllers;


use enums\StatusCodes;
use exceptions\Exception;
use mapper\GradesTable;
use enums\Roles;

class GradesController extends BaseController
{
    /**
     * GradesController constructor.
     * @param $uri
     * @param null $param
     */
    public function __construct($uri, $param = null)
    {
        parent::__construct($uri, $param);
    }

    /**
     * @return bool
     */
    public function indexAction(){
        $gt = new GradesTable();
        $data = $gt->getAllGrades();
        return $this->restOutputJSON($data);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function mygradesAction () {
        if($GLOBALS['user']->getRole() === Roles::TEACHER) {
            $data = [];
            if(!is_null($GLOBALS['user']->getUserId())) {
                $gt = new GradesTable();
                $data = $gt->getAllGrades($GLOBALS['user']->getUserId());
            }
            return $this->restOutputJSON($data);
        }
        throw new Exception('You don\'t have permission to do that.', StatusCodes::UNAUTHORIZED);
    }

}