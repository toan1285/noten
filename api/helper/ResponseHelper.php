<?php
/**
 * Created by PhpStorm.
 * User: toan
 */

namespace helper;
use enums\StatusCodes;

class ResponseHelper {

    private $body = '';
    private $statusCode = StatusCodes::OK;
    private $responseHeader = [];

    /**
     * @param $body
     */
    public function setBody($body) {
        $this->body = $body;
    }

    /**
     * @param $header
     * @param $value
     */
    public function registerHeader($header, $value) {
        $this->responseHeader[$header] = $value;
    }

    /**
     * @param $code
     */
    public function setStatusCode($code) {
        $this->statusCode = $code;
    }

    /**
     * @return string
     */
    public function returnResponse () {
        http_response_code($this->statusCode);
        foreach($this->responseHeader as $headerfield => $value) {
            header($headerfield.':'.$value);
        }
        return $this->body;
    }
}