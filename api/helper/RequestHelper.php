<?php
/**
 * Created by PhpStorm.
 * User: toan
 */

namespace helper;
use enums\Config;
use enums\MimeTypes;

class RequestHelper
{


    private $url_elements;
    private $method;
    private $requestHeader;
    private $parameters;
    private $requestFormat;

    /**
     * getter/setter
     */

    public function getParameters()
    {
        return $this->parameters;
    }

    public function getUrlElements()
    {
        return $this->url_elements;
    }

    public function getMethod()
    {
        return $this->verb;
    }

    public function getRequestHeaders()
    {
        return $this->requestHeader;
    }

    public function getRequestHeader( $header )
    {
        return isset($this->requestHeader[$header]) ? $this->requestHeader[$header] : '';
    }

    public function getRequestFormat() {
        return $this->requestFormat;
    }

    /**
     * @param $uri
     * @return mixed|string
     */
    private function getCleanedUri($uri) {
        preg_match('/\?.*/', $uri, $match);
        $uri = str_replace($match, '', $uri);
        if(substr($uri, -1, 1) === '/') {
            $uri = substr($uri, 0, -1);
        }
        return $uri;
    }

    /**
     * RequestHelper constructor.
     */
    public function __construct()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $uri = $this->getCleanedUri(str_replace(Config::API_PREFIX_BEFORE, Config::API_PREFIX_AFTER , $_SERVER['REQUEST_URI']));
        $this->url_elements = explode('/', $uri);
        $this->requestHeader = getallheaders();
        $this->parseIncomingParameters();

        // currently we will only respond with application/json
        $this->requestFormat = MimeTypes::JSON;

        // set Global User if token and user was sent, we need that later on
        if(isset($this->parameters['token']))  {
            $GLOBALS['user']->setToken($this->parameters['token']);
            $GLOBALS['user']->setUser($this->parameters['user']);
        }
        return true;
    }

    /**
     * sets global parameters from Request, fetching post data from json or x-www-form-urlencoded Request Content Type
     */
    public function parseIncomingParameters() {
        $parameters = [];

        //get
        if(isset($_SERVER['QUERY_STRING'])) {
            parse_str($_SERVER['QUERY_STRING'], $parameters);
        }
        //post -> overrides get
        $body = @file_get_contents('php://input');

        if(isset($_SERVER['CONTENT_TYPE'])){
            switch(strtolower($_SERVER['CONTENT_TYPE'])) {

                case (strtolower(MimeTypes::JSON)):

                    $body_params = json_decode($body);
                    if($body_params) {
                        foreach($body_params as $param_name => $param_value) {
                            $this->parameters[$param_name] = $param_value;
                        }
                    }
                    $this->requestFormat = MimeTypes::JSON;
                    break;

                case (strtolower(MimeTypes::URL_ENCODE)):
                    parse_str($body, $postvars);
                    foreach($postvars as $field => $value) {
                        $this->parameters[$field] = $value;

                    }
                    $this->requestFormat = MimeTypes::TEXT_HTML;
                    break;

                default:
                    // we could parse other supported formats here
                    break;
            }
        }
    }
}