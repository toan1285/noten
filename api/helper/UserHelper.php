<?php
/**
 * Created by PhpStorm.
 * User: toan
 */

namespace helper;


use mapper\BaseTable;

class UserHelper {

    private $token;
    private $user;
    private $user_id;
    private $role;

    public function getToken()
    {
        return $this->token;
    }

    public function setToken($token)
    {
        $this->token = $token;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getUserId()
    {
        return $this->user_id;
    }



    public function hasValidToken() {
        $bt = new BaseTable();
        $query = 'SELECT id, role FROM user WHERE user = ?';
        $u = $bt->select($query, [$this->user])[0];
        if(!empty($u) && isset($u['id'])) {
            $this->user_id = $u['id'];
            $this->role = $u['role'];
            $query = 'SELECT * FROM token WHERE token = ? and user_id = ?';
            $tt = $bt->select($query, [$this->token, $u['id']]);
            if(isset($tt[0]) && !empty($tt[0]) && $tt[0]['time'] >= time()) {
                return true;
            }
        }
        return false;
    }


}