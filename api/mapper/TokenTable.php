<?php
/**
 * Created by PhpStorm.
 * User: toan
 */

namespace mapper;


class TokenTable extends BaseTable {

    private $tableName = 'token';
    private $token;

    /**
     * TokenTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null $id
     * @return mixed
     */
    public function getUserToken($id = NULL) {
        if($id !== NULL) {
            $query = 'SELECT token FROM '.$this->tableName.' WHERE user_id = ?';
            return $this->select($query, [$id])[0]['token'];
        } else {
            return $this->token;
        }
    }

    /**
     * @param $id
     */
    public function createUserToken($id) {
        self::deleteOldUserTokens();
        $token = uniqid();
        $query = 'SELECT id FROM '.$this->tableName.' WHERE user_id = ?';
        if (!empty($this->select($query, [$id]))) {
            $query = 'UPDATE '.$this->tableName.' SET time = ?, token = ? WHERE user_id = ?';
        } else {
            $query = 'INSERT INTO '.$this->tableName.' (time, token, user_id) VALUES (?,?,?)';
        }
        // 900 seconds valid Time for token ~ 15 minutes
        $t = time() + 900;
        $this->select($query, [$t, $token, $id]);
        $this->token = $token;
    }

    /**
     * just for deleting old user tokens from db , increases security a bit
     */
    public function deleteOldUserTokens() {
        $query = 'DELETE FROM '.$this->tableName.' WHERE time < ?';
        $this->select($query, [time()]);
    }

}