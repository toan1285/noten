<?php
/**
 * Created by PhpStorm.
 * User: FIA15A-DNoeller
 */

namespace mapper;


class GradesTable extends BaseTable
{
    private $tableName = "grades";
    private $u = [
        'shortname' => NULL,
        'name' => NULL,
        'teacher' => NULL,
    ];

    /**
     * GradesTable constructor.
     */
    public function __construct(){
        parent::__construct();
    }

    /**
     * @param null $id
     * @param null $gradeId
     * @return array
     */
    public function getAllGrades( $id = null, $gradeId = null ){
        $query = 'SELECT * FROM  '.$this->tableName;
        if(!is_null($id) && is_null($gradeId)) {
            $query .= ' WHERE teacher = ?';
            $data = $this->select($query, [$id]);
        } else if(!is_null($id) && !is_null($gradeId)){
            $query .= ' WHERE teacher = ? AND id = ?';
            $data = $this->select($query, [$id, $gradeId]);
        } else {
            $query .= ' ORDER BY teacher';
            $data = $this->select($query);
        }
        return $data;

    }

    /**
     * @param array $grade
     */
    public function createGrades(Array $grade = []){
        $grade = array_merge($this->u, $grade);
        $query = 'INSERT INTO '.$this->tableName.'(shortname,name,teacher)VALUES(:shortname,:name,:teacher)';
        $this->insert($query,$grade);

    }

    /**
     * @param array $grades
     */
    public function updateGrades(Array $grades=[]){
        $grades=array_merge($this->u,$grades);
        $query = 'Update' . $this->tableName.' SET :shortname,:name,:teacher WHERE :id';
        $this->update($query,$grades);
        }

    /**
     * @param $id
     */
    public function deleteGrades($id){
        if(!is_null($id)) {
            $query = 'DELETE FROM '.$this->tableName.' WHERE id = ?';
            $this->delete($query, [$id]);
        }
    }
}