<?php
/**
 * Created by PhpStorm.
 * User: fia15a-tansorg
 */

namespace mapper;


use enums\Roles;
use enums\StatusCodes;
use exceptions\Exception;

class PupilsTable extends BaseTable
{
    private $tableName = 'pupil';

    /**
     * PupilsTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null $gradeId
     * @return array
     * @throws Exception
     */
    public function getPupils($gradeId = null) {
        $data = [];
        $id = $GLOBALS['user']->getUserId();
        if($id !== null) {
            switch ($GLOBALS['user']->getRole()) {
                case (Roles::TEACHER):
                    $gt = new GradesTable();
                    if($gradeId !== null) {
                        $data = $gt->getAllGrades($id, $gradeId);
                    } else {
                        $data = $gt->getAllGrades($id);
                    }
                    for ($i = 0; $i < count($data); $i++) {
                        $query = 'SELECT * FROM grade_has_pupil LEFT JOIN user ON (grade_has_pupil.pupil_id = user.id) WHERE grade_has_pupil.grade_id = ?';
                        $p = $this->select($query, [$data[$i]['id']]);
                        for ($j = 0; $j < count($p); $j++) {
                            $query = "SELECT * FROM score LEFT JOIN score_desc ON (score.desc_id = score_desc.id) WHERE pupil_id = ? AND grade_id = ?";
                            $scores = $this->select($query, [$p[$j]['pupil_id'], $p[$j]['grade_id']]);

                            for($k = 0; $k < count($scores); $k++ ) {
                                unset($scores[$k]['pupil_id'], $scores[$k]['id'], $scores[$k]['grade_id']);

                            }
                            $p[$j]['scores'] = $scores;
                            unset($p[$j]['pass'], $p[$j]['lastlogin'], $p[$j]['role'], $p[$j]['user'], $p[$j]['pupil_id'], $p[$j]['grade_id']);
                        }
                        $data[$i]['pupils'] = $p;
                    }
                    break;
                case (Roles::COMPANY):
                    $query = 'SELECT * FROM ' . $this->tableName . ' LEFT JOIN user ON (pupil.user_id = user.id) WHERE company = ? ';
                    $data = $this->select($query, [$id]);
                    for($i = 0; $i < count($data); $i++) {
                        $query = "SELECT * FROM score LEFT JOIN score_desc ON (score.desc_id = score_desc.id) LEFT JOIN grades ON (score.grade_id = grades.id) WHERE pupil_id = ? ";
                        $sc = $this->select($query,[$data[$i]['user_id']]);
                        for($j = 0; $j < count($sc); $j++) {
                            unset($sc[$j]['grade_id'], $sc[$j]['teacher'], $sc[$j]['shortname'], $sc[$j]['score_id'], $sc[$j]['pupil_id'], $sc[$j]['desc_id'], $sc[$j]['id']);
                        }
                        $data[$i]['scores'] = $sc;
                        unset($data[$i]['company'], $data[$i]['user'], $data[$i]['user_id'], $data[$i]['pass'], $data[$i]['role'], $data[$i]['lastlogin']);
                    }
                    break;
            }

            return $data;
        }
        throw new Exception('No valid User', StatusCodes::BAD_REQUEST);

    }

    /**
     * @param $id
     * @param array $vals
     * @return bool
     */
    public function editPupil($id, Array $vals = []) {
        if(isset($vals['company'])) {
            $query = 'Update '.$this->tableName.' SET company = ? WHERE user_id = ?';
            $this->update($query, [$vals['company'], $id]);
        }
        $query = "SHOW COLUMNS FROM user";
        $cols = $this->select($query);

        $query = 'UPDATE user SET ';
        $first = true;
        foreach($cols as $c) {
            if(isset($vals[$c['Field']]) && $c['Field'] !== 'user' && $c['Field'] !== 'id' && $c['Field'] !== 'pass' && $c['Field'] !== 'role' ) {
                if(!$first) {
                    $query .= ', ';
                }
                $first = false;
                $query .= $c['Field'].'=\''.$vals[$c['Field']].'\'';
            }
        }
        $query .= ' WHERE id=?';
        $this->update($query, [$id]);

        if(isset($vals['newpass']) && $vals['newpass'] !== null && $vals['newpass'] !== '') {
            $query = 'Update user SET pass = ? WHERE id = ?';
            $this->update($query, [sha1($vals['newpass']), $id]);
        }

        return true;
   }

}