<?php
/**
 * Created by PhpStorm.
 * User: toan
 */

namespace mapper;
use enums\Config;
use enums\StatusCodes;
use exceptions\Exception;

class BaseTable extends \PDO
{
    /**
     * BaseTable constructor.
     * @throws Exception
     */
    public function __construct()
    {
        // establish pdo db connection
        try {
            parent::__construct('mysql:host=' . Config::DB_SERVER . ';dbname=' . Config::DB_NAME . ';charset=utf8', Config::DB_USER, Config::DB_PASS);

        } catch(Exception $ex) {
            throw new Exception('Can\'t connect to Database.', StatusCodes::SERVICE_UNAVAILABLE);
        }
    }

    /**
     * @param $query
     * @param array $vals
     * @return \PDOStatement
     */
    private function request($query, Array $vals = []) {
        $statement = $this->prepare($query);
        $statement->execute($vals);
        return $statement;
    }

    /**
     * @param $query
     * @param array $vals
     * @return array
     */
    public function select($query, Array $vals = []) {
        $statement = $this->request($query, $vals);
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param $query
     * @param array $vals
     * @return bool
     */
    public function delete($query, Array $vals = []) {
        $statement = $this->prepare($query);
        return $statement->execute($vals);
    }

    /**
     * @param $query
     * @param array $vals
     * @return bool
     */
    public function update($query, Array $vals = []) {
        $statement = $this->prepare($query);
        return $statement->execute($vals);
    }

    /**
     * @param $query
     * @param array $vals
     * @return bool
     */
    public function insert($query, Array $vals = []) {
        $statement = $this->prepare($query);
        return $statement->execute($vals);
    }
}