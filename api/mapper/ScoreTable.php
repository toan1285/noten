<?php
/**
 * Created by PhpStorm.
 * User: toan
 */

namespace mapper;


class ScoreTable extends BaseTable
{

    private $tableName = 'score';

    /**
     * ScoreTable constructor.
     */
    public function __construct(){
        parent::__construct();
    }

    /**
     * @param $id
     * @return array
     */
    public function getScore($id){
        $query = 'SELECT * FROM grade_has_pupil LEFT JOIN grades ON (grade_has_pupil.grade_id = grades.id) WHERE pupil_id = ?';
        $data = $this->select($query, [$id]);
        for ($i = 0; $i < count($data); $i++) {
            $query = 'SELECT * FROM ' . $this->tableName.' WHERE pupil_id=? AND grade_id=?';
            $s = $this->select($query, [$id, $data[$i]['grade_id']]);
            for ($j = 0; $j < count($s); $j++) {
                $query = 'SELECT * FROM `score_desc` WHERE id = ?';
                $desc = $this->select($query, [$s[$j]['desc_id']]);
                $s[$j]['desc'] = $desc[0]['desc'];
                unset($s[$j]['score_id'], $s[$j]['pupil_id'], $s[$j]['grade_id'],  $s[$j]['desc_id'], $s[$j]['id'], $s[$j]['shortname'], $s[$j]['teacher']);
            }
            $data[$i]['scores'] = $s;
            unset($data[$i]['teacher'], $data[$i]['id'], $data[$i]['pupil_id'], $data[$i]['shortname']);
        }
        return $data;
    }

    /**
     * @param array $score
     * @return bool
     */
    public function createScore(Array $score = []) {
        $query = 'INSERT INTO '.$this->tableName.' (pupil_id, grade_id, score, desc_id) VALUES (:pupil_id,:grade_id,:score,:score_desc)';
        $data = $this->insert($query, $score);
        return $data;
    }

    /**
     * @param array $score
     * @return bool
     */
    public function editScore(Array $score = []) {
        $query = 'UPDATE '.$this->tableName.' SET score=:score WHERE score_id=:score_id ';
        return $this->update($query, $score);
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteScore($id){
        if(!is_null($id)) {
            $query = 'DELETE FROM '.$this->tableName.' WHERE score_id = ?';
            $data = $this->delete($query, [$id]);
        }
        return $data;
    }

    /**
     * @param array $grades
     * @return array
     */
    public function getTestsFromGrades(Array $grades = []) {
        $tests = [];
        foreach ($grades as $grade) {
            $query = 'SELECT * FROM score_desc WHERE grade_id = ?';
            array_push($tests, $this->select($query, [$grade]));
        }
        return $tests;
    }

    /**
     * @param $id
     * @param $desc
     * @return bool
     */
    public function createTest($id, $desc) {
        $query = 'INSERT INTO `score_desc`(`desc`, `grade_id`) VALUES (?,?)';
        return $this->insert($query, [$desc, $id]);
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteTest($id) {
        $query = 'DELETE FROM `score_desc` WHERE id = ?';
        return $this->delete($query, [$id]);
    }
}