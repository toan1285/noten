<?php
/**
 * Created by PhpStorm.
 * User: toan
 */

namespace mapper;


class UserTable extends BaseTable
{

    private $tableName = 'user';
    private $u = [
                'firstname' => NULL,
                'lastname' => NULL,
                'user' => NULL,
                'pass' => NULL,
                'role' => NULL,
            ];

    /**
     * UserTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $user
     * @param $pass
     * @return mixed
     */
    public function login($user, $pass){
        $query = 'SELECT id,firstname,lastname,user,role FROM '.$this->tableName.' WHERE user = ? AND pass = ?';
        return $this->select($query, [$user, sha1($pass)])[0];
    }

    /**
     * @param array $user
     * @return bool
     */
    public function createUser(Array $user = []) {
        $user = array_merge($this->u, $user);
        if(!is_null($user['pass']) && !is_null($user['user'])) {
            $user['pass'] = sha1($user['pass']);
            $query = 'INSERT INTO '.$this->tableName.' (firstname,lastname,user,pass,role) VALUES (:firstname,:lastname,:user,:pass,:role)';
            return $this->insert($query, $user);
        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteUser($id) {
        if(!is_null($id)) {
            $query = 'DELETE FROM '.$this->tableName.' WHERE id = ?';
            return $this->delete($query, [$id]);
        }
    }

}