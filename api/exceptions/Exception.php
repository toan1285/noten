<?php
/**
 * Created by PhpStorm.
 * User: toan
 */

namespace exceptions;

class Exception extends \Exception
{
    /**
     * Exception constructor.
     * @param string $message
     * @param int $statusCode
     */
    public function __construct($message, $statusCode)
    {
        parent::__construct($message, $statusCode);

    }
}
