<?php
/**
 * Created by PhpStorm.
 * User: toan
 */

namespace enums;


class Config extends Enum {

    const API_PREFIX_BEFORE = 'noten/api';
    const API_PREFIX_AFTER = 'controllers';

    const DB_SERVER = 'localhost';
    const DB_USER  = 'root';
    const DB_PASS = '';
    const DB_NAME = 'noten';

}