<?php
/**
 * Created by PhpStorm.
 * User: toan
 */

namespace enums;


class HeaderFields extends Enum {

    const CONTENT_TYPE = 'Content-Type';
    const CONTENT_LANGUAGE = 'Content-Language';

}