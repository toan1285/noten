<?php
/**
 * Created by PhpStorm.
 * User: toan
 */

namespace enums;


class Methods extends Enum {

    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const DELETE = 'DELETE';
    const HEAD = 'HEAD';
    const OPTIONS = 'OPTIONS';

}