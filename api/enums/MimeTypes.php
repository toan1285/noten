<?php
/**
 * Created by PhpStorm.
 * User: toan
 */

namespace enums;


class MimeTypes extends Enum
{
    const JSON          = 'application/json';
    const XML           = 'application/xhtml+xml';
    const URL_ENCODE    = 'application/x-www-form-urlencoded';
    const TEXT_HTML     = 'text/html';
    const TEXT_PLAIN    = 'text/plain';
}