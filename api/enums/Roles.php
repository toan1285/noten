<?php
/**
 * Created by PhpStorm.
 * User: fia15a-tansorg
 */

namespace enums;


class Roles extends Enum
{

    const ADMIN = "4";
    const TEACHER = "3";
    const PUPIL = "2";
    const COMPANY = "1";

}